import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrawingLibModule } from '../../projects/drawing-lib/src/public-api';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, DrawingLibModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
