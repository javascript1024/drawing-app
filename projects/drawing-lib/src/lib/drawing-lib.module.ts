import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DrawingLibComponent } from './drawing-lib.component';
import { BoardComponent } from './board/board.component';
import { ItemComponent } from './item/item.component';
import { NoteComponent } from './item/note/note.component';
import { ZoomerDirective } from './zoomer.directive';

@NgModule({
  declarations: [
    DrawingLibComponent,
    BoardComponent,
    ItemComponent,
    NoteComponent,
    ZoomerDirective,
  ],
  imports: [BrowserModule],
  exports: [DrawingLibComponent],
})
export class DrawingLibModule {}
