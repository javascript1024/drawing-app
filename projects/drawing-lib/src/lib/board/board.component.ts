import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
} from '@angular/core';
import { filter } from 'rxjs/operators';
import { DrawingService } from '../drawing.service';
import { Line } from '../models/line';
import { Point } from '../models/point';

@Component({
  selector: 'g [drawing-board]',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoardComponent implements OnInit {
  @Input() width = 0;
  @Input() height = 0;

  private lineSpace = 80;
  private scale = 1;
  constructor(
    private drawingService: DrawingService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get widthWithScale(): number {
    return this.width / this.scale;
  }

  get heightWithScale(): number {
    return this.height / this.scale;
  }

  get leftPadding() {
    return this.widthWithScale - this.width;
  }

  get topPadding() {
    return this.heightWithScale - this.height;
  }

  ngOnInit() {
    this.drawingService.scale.pipe(filter(scale => scale < 1)).subscribe(scale => {
      this.scale = scale;
      this.changeDetectorRef.markForCheck();
    });
  }

  get horizontalLines(): Line[] {
    const lineLength = this.widthWithScale + this.topPadding;
    return buildRange(-this.topPadding, this.heightWithScale, this.lineSpace).map(y =>
      Line.buildHorizontalLine(new Point(-this.leftPadding, y), lineLength)
    );
  }

  get verticalLines(): Line[] {
    const lineLength = this.heightWithScale + this.leftPadding;
    return buildRange(-this.leftPadding, this.widthWithScale, this.lineSpace).map(x =>
      Line.buildVerticalLine(new Point(x, -this.topPadding), lineLength)
    );
  }
}

// 基于 0 点为向正负两端生成数组
function buildRange(start: number, end: number, space: number): number[] {
  const range: number[] = [];
  let x = 0;
  while (x <= end) {
    range.push(x);
    x += space;
  }
  x = -space;
  while (x >= start) {
    range.push(x);
    x -= space;
  }
  return range;
}
