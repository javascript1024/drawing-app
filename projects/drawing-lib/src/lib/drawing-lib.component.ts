import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  HostListener,
  ChangeDetectorRef,
} from '@angular/core';
import { DrawingService } from './drawing.service';
import { Area } from './models/area';

@Component({
  selector: 'drawing-lib',
  templateUrl: './drawing-lib.component.html',
  styleUrls: ['./drawing-lib.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DrawingLibComponent implements OnInit {
  private _width = 0;
  private _height = 0;

  @Input() set width(val) {
    this._width = val;
  }

  get width(): number {
    return this.isFullScreen ? window.innerWidth : this._width;
  }

  @Input() set height(val) {
    this._height = val;
  }

  get height(): number {
    return this.isFullScreen ? window.innerHeight : this._height;
  }

  @Input() isFullScreen;

  constructor(
    public drawingService: DrawingService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.drawingService.drawingArea.next(new Area(this.width, this.height));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.changeDetectorRef.markForCheck();
  }
}
