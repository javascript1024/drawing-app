import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "drawing-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
