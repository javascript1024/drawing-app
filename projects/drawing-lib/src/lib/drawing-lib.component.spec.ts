import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DrawingLibComponent } from "./drawing-lib.component";

describe("DrawingLibComponent", () => {
  let component: DrawingLibComponent;
  let fixture: ComponentFixture<DrawingLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DrawingLibComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
